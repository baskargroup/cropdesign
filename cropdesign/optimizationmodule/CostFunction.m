% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function is used to define and evaluate the costfunction/objective functions
function value = CostFunction(Traits,theta,hmax,AnMax,Irrfre)

% initialize variables
trans_w=0;trans_d=0;yield_w=0;yield_d=0;survival_w=0;survival_d=0;value1=0;
value2=0;

% Ensure that gmax is higher than gmin
if (Traits(:,1)>Traits(:,2))
    value=realmax;
    Traits(:,1);
    Traits(:,2);
    return
end

% Objective 1: maximum productivity under irrigation
%Read weather file
filename = 'Weather.csv';
AllWeather = ReadWeatherFile(filename);
%set input parameters
filename = 'InputParam.csv';
row2read= 2; %row 1 is the header 
InputParams = ReadInputParams4Design(filename,row2read,Traits);
InputParams.Irrigate=Irrfre;

%set global constants
GlobalConsts = SetGlobalConsts();

%solve 
resultsA = CropSimulator(AllWeather, InputParams, GlobalConsts);
results = resultsA{1};
CropDied = resultsA{2};
Error_data1=resultsA{3};
%postprocess results
ppresults = PostProcResults(results);
trans_w= ppresults(1)*3600*1e-3;
yield_w= ppresults(2)*3600*1e-6;
survival_w=CropDied(2);
% objective function: maxproductivity under irrigation
value1 = -ppresults(2)./AnMax;
%end
 
% Objective 2: maximum productivity under irrigation
%Read weather file
filename = 'Weather.csv';
AllWeather = ReadWeatherFile(filename);

%set input parameters
filename = 'InputParam.csv';
row2read= 2; %row 1 is the header
InputParams = ReadInputParams4Design(filename,row2read,Traits);
InputParams.Irrigate=0;

%set global constants
GlobalConsts = SetGlobalConsts();

%solve 
resultsA = CropSimulator(AllWeather, InputParams, GlobalConsts);
results = resultsA{1};
CropDied = resultsA{2};
Error_data2=resultsA{3};
%postprocess results
ppresults = PostProcResults(results);
Lifetime =  CropDied(2);
trans_d= ppresults(1)*3600*1e-3;
yield_d= ppresults(2)*3600*1e-6;
survival_d=CropDied(2);
% objective function: maxproductivity under no irrigation
value2 = -(ppresults(2)./AnMax);

format short e
Data1=Traits;
Data2=[yield_w,trans_w,survival_w,yield_d,trans_d,survival_d];
% Display errors and traits for postprocessing. The displayed values will
% be saved in 

Data=[Data1 Data2 Error_data1 Error_data2]

%Final objective funciton
s1 = cos(theta);
s2 = sin(theta);
value=s1^2.*value1+s2^2.*value2;
                    


 