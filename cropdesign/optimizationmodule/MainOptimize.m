% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function setup the genetic algrothim variables and selected traits
% for optimization. This function also allows the code to run parallelly in
% mulitple processors. 
function MainOptimize(theta1,Irrfre,gennum,popsize,stallgen,tolfun,itr1,itr2,condition,nop)

addpath inputs
addpath src

theta=str2double(theta1)*pi/180; % theta is the weighting factor between two preliminary objective functions 
gennum=str2double(gennum); % maximum of generations, as stopping criteria. 
popsize=str2double(popsize); % initial population.
itr1=str2double(itr1);% start iteration % iteration number will help to create different seeds
itr2=str2double(itr2);% Finish iteration 
condition=str2double(condition);% bound or unbound traits space for conductactivity
nop=str2double(nop);% number of processors used 
stallgen=str2double(stallgen); % no of stall generations that occur as a stopping criteria
Irrfre=str2double(Irrfre); % irrigation frequency
tolfun=str2double(tolfun); % Tolerance 

parpool ('local',nop);

hourMax=1464; % Total time period in hours
AnMax=25000; % Maximum photosynthesis used for normalization, umol/m2
nvars=7; 
 
 switch condition
      case 0        
% control variables are gmin, gmax, Slwp, Phi_th, Srwp, Kleaf+stem, Kroot
 lb=[1e-2 1e-2  0 -1.33 0  0.1  0.1]; % lower limit
 ub=[0.25 3 89.99 -0.06 89.99 5e3  1e4]; % upper limit
    
      case 1 % upper limit of hydraulic conductance of leaf+stem and root are bounded by typical existing upper limits of those values.
 lb=[1e-2 1e-2  0 -1.33 0  0.1  0.1]; % lower limit
 ub=[0.25 3 89.99 -0.06 89.99 50  100]; % upper limit
  end
  
% linear inequality
 Aineq=[1 -1 0 0 0 0 0];
 bineq=-0.01;
 
Generations_Data=gennum; % generations number
PopulationSize_Data=popsize; % population size
StallGenLimit_Data=stallgen;
TolFun_Data=tolfun;
CrossoverFraction_Data=0.4;
 
%% Start with the default options
options = gaoptimset; 
% Modify options setting
options = gaoptimset(options,'CrossoverFraction', CrossoverFraction_Data);
options = gaoptimset(options,'PopulationSize', PopulationSize_Data);
options = gaoptimset(options,'CreationFcn', @gacreationlinearfeasible);
options = gaoptimset(options,'MigrationDirection', 'both');
options = gaoptimset(options,'Generations', Generations_Data);
options = gaoptimset(options,'SelectionFcn', @selectionroulette);
options = gaoptimset(options,'CrossoverFcn', {  @crossoverheuristic 1.1 });
options = gaoptimset(options,'MutationFcn', {  @mutationadaptfeasible });
options = gaoptimset(options,'HybridFcn', {  @fmincon [] });
options = gaoptimset(options,'StallGenLimit', StallGenLimit_Data);
options = gaoptimset(options,'TolFun', TolFun_Data);
options = gaoptimset(options,'Display', 'off');
options = gaoptimset(options,'PlotFcns', {  @gaplotbestf @gaplotbestindiv @gaplotdistance @gaplotrange @gaplotscorediversity @gaplotscores @gaplotselection @gaplotstopping });
options = gaoptimset(options,'Vectorized', 'off');
options = gaoptimset(options,'UseParallel', 1 );

i=itr1;
while i<=itr2 %repeatability  
rng(i);

        [x,fval,exitflag,output,population,score] = ga(@(x) CostFunction(x,theta,hourMax,AnMax,Irrfre),nvars,Aineq,bineq,[],[],lb,ub,[],[],options);  
        fprintf('The best function value found was : %g\n', fval);  
        filename = sprintf('Data_%s_%d_%d',theta1,gennum,i);
        save(filename);
        filename1 = sprintf('Graphs_%s_%d_%d',theta1,gennum,i);
        savefig(filename1);
        clear x fval exitflag output population scores;   
        iteration=i
i=i+1;         
end

delete(gcp);
quit

end



