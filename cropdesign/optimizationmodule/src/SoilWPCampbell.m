% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function calulate saturated soil water potential based on soil type
function x = SoilWPCampbell(InputParams,Theta)

SoilWPSat = InputParams.SoilWPSat;
Soilb = InputParams.Soilb;
ThetaSat = InputParams.ThetaSat;

% Soil water potential function of theta(outputs in MPa)
x=SoilWPSat*((ThetaSat/Theta)^Soilb);
%     # Note the SoilWPSat should be in MPa units (often in kPa in the literature)
%Output: MPa
