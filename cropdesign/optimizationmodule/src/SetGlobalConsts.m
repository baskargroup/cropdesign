% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function is used to define some global constants

function GlobalConsts = SetGlobalConsts()
%ppm CO2 concentration in atmosphere
GlobalConsts.Ca=400; 
GlobalConsts.emmiss=0.97;

%absorptance of leaf to IR radiation
GlobalConsts.Air=0.96; 
%J mol-1 C-1
GlobalConsts.Cp=29.3; 
%mol-1  =-42.914*ToC+ 45064 J mol-1 from Nobel (1999)
%(=2.501-0.002361*Tleaf in MJ kg-1 from CIMIS)
GlobalConsts.Latent=43500; 
%W m-2 K-4
GlobalConsts.SBconst=5.67*10^-8; 
%albedo=0.25 #Norman and Campbell table 11.2 for grass CANOPY    OR
GlobalConsts.albedo=0.5; %Norman and Campbell table 11.4 for BEAN LEAF
