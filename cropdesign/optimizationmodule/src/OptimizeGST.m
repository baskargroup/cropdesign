% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function finds gst that satisfes three numerically solved variables Tleaf, LWP and
% Ci those statisfy ,respectively, energy, water transport and CO2
% transport in the leaf.

function f = OptimizeGST(gst,paramcell)
 
InputParams = paramcell{1};
GlobalConsts = paramcell{3};
HourlyConstants = paramcell{4};

%setting optimization parameters
options = optimset('fminbnd');
options = optimset(options,'MaxIter',2000,'MaxFunEvals',2000,'TolFun',1e-4);

paramcell{7} = gst;%%
% Find Tleaf satistying enegry balace on leaf given gst
[Tleaf] = fminbnd(@(x) EBsquared(x,paramcell),-10,100,options);%%

paramcell{8} = Tleaf;%%
%Find LWP minimizing (TRSupply-TRDemand)^2 given gst, Tleaf
[LWP] = fminbnd(@(x) TRsquared(x, paramcell),-5,0,options);%%
paramcell{9} = LWP;%%

%Find Ci, An minimizing (CO2 supply-CO2 demand)^2 given gst, Tleaf, LWP
Ca = GlobalConsts.Ca;
[Ci] = fminbnd(@(x) AnC4squared(x, paramcell), 0,Ca*2,options);%%
gblc = HourlyConstants.gblc;
An=FicksAnC4(Ca,Ci,gblc,gst);

gmin = InputParams.gmin;
gmax = InputParams.gmax;
gstSensitivity = InputParams.gstSensitivity;
gstThres = InputParams.gstThres;
gstABAsens = InputParams.gstABAsens;
gmaxAnP1 = InputParams.gmaxAnP1;
gmaxAnP2 = InputParams.gmaxAnP2;

%get RootWP
[TRdemand, SoilWpatRoota, RootWP] = GetRootWP(paramcell);

% Find updated gst
gstnew = gstfunc(gmin,gmax,LWP,gstSensitivity,gstThres,gstABAsens,RootWP,An,gmaxAnP1,gmaxAnP2);
f = (gst-gstnew)^2/(min(gstnew+eps,gst+eps))^2;



