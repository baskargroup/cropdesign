% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This funciton evaluates soil water potential based on the soil properties
% including texture, b, water holding capacity, Ksat

function x = ksoilCampbell(SoilKoff,Ksat,SoilWPSat,SoilWP,SoilWPatRoota,Soilb,RootLengthDensity,SoilDepth,SoilRadius,LAI,RootRadius)


Xconv=2*pi*SoilDepth*RootLengthDensity/(LAI*log((1/(pi*RootLengthDensity)^0.5)/RootRadius) );   %# m of root per m2 of ground

if(SoilKoff==1)
    x=Xconv*Ksat*1000;
else
    x=Xconv*Ksat*1000*((SoilWPSat/(-sqrt(SoilWP*SoilWPatRoota)))^(2+3/Soilb)); % Xconv is the conversion factor between conductance and conductivity
end
%in soil conductance units mmol m-2 leaf area s-1 MPa-1

