% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function calcuate the CO2 diffuses into leaf 
function An = FicksAnC4(Ca,Ci,gblc,gst)

gtotal=(1/(gblc/1.37)+1/(gst/1.6))^-1; % 1.37 and 1.6 are the conversion factors for diffusivities.
An=gtotal*(Ca-Ci); % 
