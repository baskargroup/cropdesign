% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function calculate water usage and productivity by summing up hourly transpriation and assimilation 
function ppresults = PostProcResults(data)

A = struct2cell(data);
B = cell2mat(A);
B = B';

%Get important data including transpiration and assimilation
TR = B(:,2);
An = B(:,11);

sumTR = sum(TR); %water usage
sumAn = sum(An); %productivity

ppresults = [sumTR sumAn];