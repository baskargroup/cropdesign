% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function is used to calculate some hourly constants based on the
% input weather data
function HourlyConstants = SetHourlyConstants(InputParams,CurrentWeather)

WindMS = CurrentWeather(4);
CropHeight = InputParams.CropHeight;
RH = CurrentWeather(3);
TaoC = CurrentWeather(2);
PARuE = CurrentWeather(6);


Uz = WindCanopy(WindMS,CropHeight); % Uz is the corrected wind velocity at top of the plant

HourlyConstants.RH=RH; 
HourlyConstants.PAR=PARuE;
% Boundary layer conductance to H20 (mol m-2 s-1) - Nobel??????
HourlyConstants.gblc = 1.4*0.147*sqrt(Uz/(0.72*InputParams.LeafDimension));
%ambient water vapor pressure (kPa) Buck? equation
HourlyConstants.ea=((RH./100).*0.6107.*exp(17.38.*TaoC./(TaoC+239)));

%Leaf boundary layer conductance to heat
HourlyConstants.gha=1.4*0.135*sqrt(Uz/(0.72*InputParams.LeafDimension));

%Electron transport rate at 25oC for the measured PAR
scratch = PARuE*InputParams.PSIIPAR+InputParams.Jmax25;
scratch2 = InputParams.curvePAR*InputParams.PSIIPAR*PARuE*InputParams.Jmax25;
HourlyConstants.J25= (scratch - sqrt(scratch^2-4*scratch2))/(2*InputParams.curvePAR);

