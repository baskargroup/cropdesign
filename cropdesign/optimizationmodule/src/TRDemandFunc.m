% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% A demand equation for transpiration derived from leaf stomatal and boundary layer conductances

function x = TRDemandFunc(gst,gblc,VPD)
x=((1/gst+1/gblc)^-1)*VPD;      %VPD in mmol mol-1
%Output: mmol m-2 s-1
