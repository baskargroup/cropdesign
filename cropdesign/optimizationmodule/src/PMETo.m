% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

%Hourly PM reference ETo for grass from Rick Snyder's ETo spreedsheet

function x = PMETo(Tair,Rn,es,ea,Press,WindMS)

delta=4099*es/((Tair+237.3)^2);       %kPa oC-2
if(Rn>0)
    G=Rn*0.1;       %# convert to MJ m-2 hr-1 from W m-2 = J m-2 s-1
    rs=50;
else
    G=Rn*0.5;
    rs=200;
end
lambda=2.501-0.002361*Tair; %MJ kg-1
gammas=(0.00163*Press)/lambda; %KPa oC-1
if(WindMS>0.5)
    ra=208/WindMS;
else
    ra=208/0.5;
end
gammasta=gammas*(1+rs/ra);
x=((1/lambda)*delta*(((Rn-G)*3600)/1000000))/(delta+gammas)+(gammas*((37/(Tair+273.16))*WindMS*(es-ea)))/(delta+gammasta);
if(x< 0)
    x=0;
end %mm hr-1
