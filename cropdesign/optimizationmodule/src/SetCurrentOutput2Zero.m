% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016
% This function initialize variables at every time step

function CurrentOutput = SetCurrentOutput2Zero()
CurrentOutput.gst=0; 
CurrentOutput.TR=0;
CurrentOutput.LeafWP=0;
CurrentOutput.StemWP=0;
CurrentOutput.RootWP=0;
CurrentOutput.SoilWPatRoot=0;
CurrentOutput.ksoil=0;
CurrentOutput.Theta=0;
CurrentOutput.ThetaPrev=0;
CurrentOutput.SoilWP=0;
CurrentOutput.An=0;
CurrentOutput.PCO=0;
CurrentOutput.CcCi=0;
CurrentOutput.VPDmmol=0;
CurrentOutput.PAR=0;
CurrentOutput.Irrigmm=0;
CurrentOutput.rs=0;
CurrentOutput.ra=0;
CurrentOutput.Tleaf=0;
CurrentOutput.Rad=0;
CurrentOutput.Latent=0;
CurrentOutput.Convec=0;
CurrentOutput.Emmit=0;
CurrentOutput.Ta=0;
CurrentOutput.ETmm=0;
CurrentOutput.PMETomm =0;