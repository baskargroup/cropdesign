% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function prepares variables/parameters at every time step to store
% the results along with the inputs

function OutputValsCurr = GetAllCurrentResults(paramcell)

InputParams = paramcell{1};
CurrentWeather = paramcell{2};
GlobalConsts = paramcell{3};
HourlyConstants = paramcell{4};
OutputValsPrev = paramcell{5};
OutputValsCurr = paramcell{6};
gst = paramcell{7};
Tleaf = paramcell{8};
LWP = paramcell{9};
Ci = paramcell{10};
An = paramcell{11};

kleaf = InputParams.kleaf;
kstem = InputParams.kstem;
kroot = InputParams.kroot;
SoilKoff = InputParams.SoilKoff;
Ksat = InputParams.Ksat;
SoilWPSat = InputParams.SoilWPSat;
Soilb = InputParams.Soilb;
RootLengthDensity = InputParams.RootLengthDensity;
SoilDepth = InputParams.SoilDepth;
SoilRadius = InputParams.SoilRadius;
LAI = InputParams.LAI;
RootRadius = InputParams.RootRadius;
SoilWP = OutputValsCurr.SoilWP;
Pressure = InputParams.Pressure; 
CropHeight = InputParams.CropHeight;
LeafDimension = InputParams.LeafDimension;
TaoC = CurrentWeather(2);
WindMS = CurrentWeather(4);
SolarRadWM2 = CurrentWeather(8);
NetRadWM2 = CurrentWeather(9);

gblc = HourlyConstants.gblc;
ea = HourlyConstants.ea;
gha = HourlyConstants.gha;
es=0.6107*exp(17.38*Tleaf/(Tleaf+239)); %Saturation vapor pressure (kPa)
VPD=(es-ea)*1000/InputParams.Pressure;  %leaf to air vapor pressure deficit (mmol H2O mol-1 air)
Latent = GlobalConsts.Latent;
Cp = GlobalConsts.Cp;
emmiss = GlobalConsts.emmiss;
SBconst = GlobalConsts.SBconst;
albedo = GlobalConsts.albedo;


[TRdemand, SoilWpatRoota, RootWPa] = GetRootWP(paramcell);% RootWa is the water potential at the root

ra = (Pressure*1000/(8.314*(TaoC+273.15)))/(1.4*0.147*sqrt(WindCanopy(WindMS,CropHeight)/LeafDimension));
rs = (1/((Pressure*gst)/(8.314*(273.15+TaoC))))/LAI;

Lat = min(0,-Latent.*((1/gst+1/gblc)^-1).*((0.6107.*exp(17.38*Tleaf./(Tleaf+239))-ea)./Pressure));
Conv=-2*Cp*gha*(Tleaf-TaoC);
Emmitt=-emmiss*SBconst*(Tleaf+273.15)^4;
SolarRad=SolarRadWM2*(1-albedo)*(1+0);

TR = TRDemandFunc(gst,gblc,VPD);
ETmm = TR*LAI*(18/1000)*3.600;

PMETomm = PMETo(TaoC,NetRadWM2,es,ea,Pressure,WindMS);

Theta = OutputValsCurr.Theta;
SoilWP = OutputValsCurr.SoilWP;
Irrigmm = OutputValsCurr.Irrigmm;

%store current results
OutputValsCurr.LeafWP=LWP;
OutputValsCurr.gst=gst; 
OutputValsCurr.TR= TR;
OutputValsCurr.PAR=CurrentWeather(6);
OutputValsCurr.An=An;
OutputValsCurr.CcCi=Ci;
OutputValsCurr.PCO=0;
OutputValsCurr.SoilWPatRoot = SoilWpatRoota;
OutputValsCurr.RootWP=RootWPa;
OutputValsCurr.ksoil=ksoilCampbell(SoilKoff,Ksat,SoilWPSat,SoilWP,SoilWpatRoota,Soilb,RootLengthDensity,SoilDepth,SoilRadius,LAI,RootRadius);
OutputValsCurr.StemWP= StemWP(RootWPa,TRdemand,kstem);
OutputValsCurr.VPDmmol=VPD;
OutputValsCurr.rs=rs;
OutputValsCurr.ra=ra;
OutputValsCurr.Tleaf=Tleaf;
OutputValsCurr.Theta = Theta;
OutputValsCurr.ThetaPrev= Theta;
OutputValsCurr.SoilWP = SoilWP;
OutputValsCurr.Irrigmm = Irrigmm;
OutputValsCurr.Emmit= Emmitt;
OutputValsCurr.Latent=Lat;
OutputValsCurr.Convec=Conv;
OutputValsCurr.Rad= SolarRad;
OutputValsCurr.Ta=TaoC;
OutputValsCurr.ETmm=ETmm;
OutputValsCurr.PMETomm =PMETomm;