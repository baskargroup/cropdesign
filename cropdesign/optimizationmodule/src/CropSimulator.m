% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

function results = CropSimulator(weatherFile, InputParams, GlobalConsts)
% It is the main function that acts as a portal to call different functions and implement
% the model.

iterirrig=0;    %Counter used to schedule irrigation if "Irrigate" is set to >0 in Input
CropDied=0;     %flag for crop death

Tstart = 1;
Tend = size(weatherFile,1); % number of time steps, e.g. number of hourly weather data

%Preallocate memory for speed
scratch = SetCurrentOutput2Zero();
OutputVals = repmat(struct(scratch),size(weatherFile,1),1);

%Preallocate memory for speed
paramcell{1} = InputParams;
paramcell{2} = weatherFile(1,:);
paramcell{3} = GlobalConsts;
HourlyConstants = SetHourlyConstants(InputParams,weatherFile(1,:));
paramcell{4} = HourlyConstants;
paramcell{5} = OutputVals(1);
paramcell{6} = OutputVals(1);
paramcell{7} = 0;
paramcell{8} = 0;
paramcell{9} = 0;
paramcell{10} = 0;
paramcell{11} = 0;

%loop over hourly weather data
%Iterates through each hour of weather in Weather file
for i=Tstart:Tend
%   for i=1:50
    
    if(i==1)
        OutputVals(1).ThetaPrev = InputParams.ThetaStart;
        OutputVals(1).Theta = InputParams.ThetaStart;
        iterirrig = 0;
        CropDied = 0;
    end
    
    %run only if crop is not dead
    if(CropDied==0)
        %Get current weather
        CurrentWeather = weatherFile(i,:);
        
        %Irrigates the soil by the sum of TR every "Irrigate" days;
        %the days are set in the ConstantList file by putting a number
        %greater than 0 in "Irrigate"; default in ConstantList is 0 and
        %this would lead to no irrigation
        iterirrig=iterirrig+1;
        if(iterirrig == 24*InputParams.Irrigate)
            iterirrig = 0; %reset count
            sumTR =  0; %%%
            for j=i-24*InputParams.Irrigate+1:i
                sumTR = sumTR+OutputVals(j).TR;
            end
            %Calculate and save the irrigation amount in mm
            Irrig=sumTR*InputParams.LAI*(18/1000)*3600*pi*InputParams.SoilRadius^2;
            %Saves the irrigation event in mm and is equal to the
            %cumulative TR since the last irrigation event
            OutputVals(i).Irrigmm=Irrig/(1000*pi*InputParams.SoilRadius^2);
        else
            OutputVals(i).Irrigmm = 0;
            Irrig = 0;
        end
        
        %Calculate Current Theta from the last hours results
        %"SoilDef" in ConstantList if set to 0 prevents Theta from changing
        %(from TR or precipitation)
        if(i>1)
            OutputValsPrev = OutputVals(i-1);
        else
            OutputValsPrev = OutputVals(1);
        end
        OutputVals(i).Theta = ThetaNow(Irrig,InputParams,CurrentWeather,OutputValsPrev);
        
        %Calculate soil water potential from Theta
        %output in MPa
        OutputVals(i).SoilWP=SoilWPCampbell(InputParams, OutputVals(i).Theta);
        
        %set hourly constant variables
        HourlyConstants = SetHourlyConstants(InputParams,CurrentWeather);
        
        %-------------
        %Find TLeaf, gst, LWP, ci, An
        %-------------
        OutputValsCurr = OutputVals(i);
        %paramcell{1} = InputParams;
        paramcell{2} = CurrentWeather;
        %paramcell{3} = GlobalConsts;
        paramcell{4} = HourlyConstants;
        paramcell{5} = OutputValsPrev;
        paramcell{6} = OutputValsCurr;
        PAR=HourlyConstants.PAR;
        
        %setting optimization parameters
        options = optimset('fminbnd');
        options = optimset(options,'MaxIter',2000,'MaxFunEvals',2000,'TolFun',1e-5);
        
 %Main steps: 
        %Find gst that satisfies three balance equations
           [gst,feval_g] = fminbnd(@(x) OptimizeGST(x,paramcell),InputParams.gmin,InputParams.gmax,options);
                   
        %postprocess to find Tleaf
        paramcell{7} = gst;
        [Tleaf,feval_t] = fminbnd(@(x) EBsquared(x,paramcell),-10,100,options);
        
        %postprocess to find LWP
        paramcell{8} = Tleaf;
        [LWP, feval_l] = fminbnd(@(x) TRsquared(x, paramcell),-5,0,options); 
        
        %postprocess to find Ci and An
        paramcell{9} = LWP;
        Ca = GlobalConsts.Ca;
        [Ci, feval_c] = fminbnd(@(x) AnC4squared(x, paramcell), 0,Ca*2,options);
        gblc = HourlyConstants.gblc;
        An=FicksAnC4(Ca,Ci,gblc,gst);
        paramcell{10} = Ci;
        paramcell{11} = An;
        
        %store results
        OutputVals(i) = GetAllCurrentResults(paramcell);
        
        % Convergences of the Tleaf, LWP, Ci and gst evaluation each time step
           func_er_t(i)=feval_t;
           func_er_l(i)=feval_l;
           func_er_c(i)=feval_c;
           func_er_g(i)=feval_g;
        % gst and LWP for each time step
           fun_gst(i)=gst;
           fun_lwp(i)=LWP;
    end
    
    %%check to see if plant died
    if(LWP<=InputParams.PermWiltPt)
        CropDied=1;
        break;
    end
    if(Tleaf>=InputParams.PermTempDamage)
        CropDied=1;
        break;
    end    
end

% store the An, Tr, days survived
results{1} = OutputVals;
results{2} = [CropDied i];
% store the maximum deviations in the Tleaf, LWP, Ci and gst evaluations 
Error_data=[max(func_er_t) max(func_er_l) max(func_er_c) max(func_er_g) min(-fun_gst) min(fun_lwp)]; 
results{3}=Error_data;

