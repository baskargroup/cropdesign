% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% The model is adapted from the plant physiology model 
% by Matthew Gilbert, UC Davis, CA. 

% add path to the folders "inputs" and "src". "Inputs" contains files of 
% weather file and plant traits, and "src" contains various functions of 
% the model implementation. 
clc;
close all;
clear all;

maindir='G:\CropDesign\FoodEnergySecurityJournal\cropdesign\cropdesign\plantphysiologymodel';
addpath (fullfile(maindir,'src'))
addpath (fullfile(maindir,'src','stomata_model'))
%  Weather and 
addpath (fullfile(maindir,'inputs'))

filename = 'Weather.csv'; % reading weather file
AllWeather = ReadWeatherFile(filename);
filename = 'InputParam.csv'; % reading plant traits

%row2read, row 1 is header
row2read = 2;
InputParams = ReadInputParams(filename,row2read);
%set global constants
GlobalConsts = SetGlobalConsts();

%solve 
resultsA = CropSimulator(AllWeather, InputParams, GlobalConsts);
results = resultsA{1};
CropDied = resultsA{2};

%store results into a csv file 
 StoreResults(results);
 
%postprocess results
ppresults = PostProcResults(results);
Transpri=ppresults(1)*3600*1e-3% in mol/m2
Assimil=ppresults(2)*3600*1e-6% in mol/m2
survival=CropDied(2)