% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016
% This funciton calculate root water potential from leaf water potnetial and plant hydraulics
function x = RootWP(SoilWPatRoota,TRdemand,kroota)
      x=-TRdemand/kroota+SoilWPatRoota;
  
