% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016
%Stem water potential calculation derived from plant hydraulics
function x =   StemWP(RootWPa,TRdemand,kstema)
      x=-TRdemand/kstema+RootWPa;
      %Output: MPa
  

