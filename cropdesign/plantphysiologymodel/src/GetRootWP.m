% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function evaluates water potentail at root baed on the leaf water
% potential and hydraulic conductance of the leaf, stem and root.

function [TRdemand, SoilWpatRoota, RootWPa] = GetRootWP(Params)

InputParams = Params{1};
CurrentWeather = Params{2};
GlobalConsts = Params{3};
HourlyConstants = Params{4};
OutputValsPrev = Params{5};
OutputValsCurr = Params{6};
gst = Params{7};
Tleaf = Params{8};
LeafWP = Params{9};

kleaf = InputParams.kleaf;
kstem = InputParams.kstem;
kroot = InputParams.kroot;
gblc = HourlyConstants.gblc;
ea = HourlyConstants.ea;

es=0.6107*exp(17.38*Tleaf/(Tleaf+239)); %Saturation vapor pressure (kPa)
VPD=(es-ea)*1000/InputParams.Pressure;  %leaf to air vapor pressure deficit (mmol H2O mol-1 air)
SoilWP = OutputValsCurr.SoilWP;
TRdemand = TRDemandFunc(gst,gblc,VPD);
SoilWpatRoota = SoilWPatRoot(LeafWP,TRdemand,kleaf,kstem,kroot,SoilWP); % SoilWpatRoota is the water potential at the root and soil interface

x = RootWP(SoilWpatRoota,TRdemand,kroot);
RootWPa = x;