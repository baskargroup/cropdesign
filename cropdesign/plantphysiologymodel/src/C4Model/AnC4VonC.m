% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016


function x =   AnC4VonC(Ci,JTleaf,VcmaxTleaf,RdTleaf,VpmaxTleaf,VprTleaf)
%Calculation of C4 An - net assimilation based on enzyme activity and
%sunlight intensity

%assigned a priori from Table 4.1 of von Caemmerer (2000) note there are no 
%temperature corrections here
Kp=80;          %ubar Michaelis Menten constant for PEP carboxylase for CO2
gs=  0.003;     %mol m-2 s-1 conductance of CO2 out through Bundle sheath wall

Rm=0.5*RdTleaf;        %umol m-2 s-1 mesophyll mitochondrial respiration

y=0.4; % Fraction of electron flux used by mesophyll cell.

Cm=Ci; %#?

Vp=min((Cm*VpmaxTleaf)/(Cm+Kp),VprTleaf);  %#maybe Vpr only important at high CO2
Ac=min(Vp+gs*Cm-Rm,VcmaxTleaf-RdTleaf);   %# First equation relates to mesophyll reactions: Vp (rate of PEP carboxylation) and leakiness
                                          %# Second equaiton relates to bundle-sheath reactions: Vcmax (rate of rubisco carboxylation) 
                                          %# RdTleaf is the rate of CO2 production from respiration.
Aj= (1-y)*JTleaf/3-RdTleaf;%# net assimilation based on available sunlight.

x=min(Ac,Aj);
