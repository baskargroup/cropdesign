% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

%This funciton find Ci by minimizing (CO2 supply by diffussion -CO2 demand by photosynthesis)^2 at a given gst, Tleaf

function x = AnC4squared(Ci,Params)
InputParams = Params{1};
CurrentWeather = Params{2};
GlobalConsts = Params{3};
HourlyConstants = Params{4};
OutputValsPrev = Params{5};
OutputValsCurr = Params{6};
gst = Params{7};
Tleaf = Params{8};

%Set temperature dependant photosynthetic parameters for C4 given Tleaf
% These parameters are evaluated by the calibrating gas exchange data. 
Vcmax25 = InputParams.Vcmax25;
Vpmax25 = InputParams.Vpmax25;
Vpr25 = InputParams.Vpr25; 
Rd25 = InputParams.Rd25;
J25 = HourlyConstants.J25;
VprQ10=InputParams.VprQ10;% #95% CI is 13.1 to 23.4 
VcmaxQ10=InputParams.VcmaxQ10;%#95% CI is 0.914 to 1.065      # A little low, but probably not a problem. The resultant temperature responses are reasonable
Vcmaxa=InputParams.Vcmaxa;% #95% CI is 0.0806 to 0.100
Vcmaxb=InputParams.Vcmaxb;% #95% CI is 31.1 to 32.4
Vcmaxc=InputParams.Vcmaxc;%#95% CI is  38.7 to 44.1
VpmaxQ10=InputParams.VpmaxQ10;%#95% CI is 1.37 to 1.52
RdQ10=InputParams.RdQ10;%#95% CI is 1.55 to 2.31
Ja=InputParams.Ja;%#95% CI is  4.93 to 6.7
Jb=InputParams.Jb;%#95% CI is 3.65 to 8.22
alpha=InputParams.alpha;%#95% CI is 0.0727 to 0.1068

%based upon Collatz et al. (1992)
VcmaxTleaf=(Vcmax25*VcmaxQ10^((Tleaf-25)/10))/((1+exp(Vcmaxa*(Vcmaxb-Tleaf)))*(1+exp(Vcmaxa*(Tleaf-Vcmaxc))));
%assumed response roughly based upon Collatz et al. (1992)
VpmaxTleaf=Vpmax25*VpmaxQ10^((Tleaf-25)/10);
%assumed response roughly based upon Collatz et al. (1992)
VprTleaf=Vpr25*VprQ10^((Tleaf-25)/10); 
%Tleaf Q10 = 2   # based upon Collatz et al. (1992)
RdTleaf=(Rd25*RdQ10^((Tleaf-25)/10));
%based upon C3 response
JTleaf=J25*exp(Ja-(Jb/(0.008314*(273.15+Tleaf))));

Ca = GlobalConsts.Ca;% in ppm, i.e. 1e-6 gram/gram
gblc = HourlyConstants.gblc;

AnFicks=FicksAnC4(Ca,Ci,gblc,gst);% CO2 supply via diffusion


AnvC=(1+alpha/2)*AnC4VonC(Ci,JTleaf,VcmaxTleaf,RdTleaf,VpmaxTleaf,VprTleaf);% CO2 demand for photosysthesis
% alpha is a fitting parameter to include perfromance variability on the leaves. 

x = 4*(AnFicks-AnvC)^2/(AnFicks+AnvC+eps)^2;  

