% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% this function find leaf temperature,Tleaf by balancing energy that exchanges via solar radiation, irradiation,
% forced convection and evaporation to and from the leaf. Here the energay storage by the leaf
% is neglected. 

function  x = EBsquared(Tleaf,Params)

InputParams = Params{1};
CurrentWeather = Params{2};
GlobalConsts = Params{3};
HourlyConstants = Params{4};

%parameters
Pressure = InputParams.Pressure;
TaoC = CurrentWeather(2) ;
SolarRadWM2 = CurrentWeather(8);
ea = HourlyConstants.ea;
gblc = HourlyConstants.gblc;
gha = HourlyConstants.gha;
Latent = GlobalConsts.Latent;
Cp = GlobalConsts.Cp;
emmiss = GlobalConsts.emmiss;
SBconst = GlobalConsts.SBconst;
albedo = GlobalConsts.albedo;
gst = Params{7};

%latent heat via evaporation of water 
Lat = min(0,-Latent*((1/gst+1/gblc)^-1)*((0.6107*exp(17.38*Tleaf/(Tleaf+239))-ea)/Pressure));

% convection heat transfer from both sides of the leaf
Conv=-2*Cp*gha*(Tleaf-TaoC);

%Emmisive radiation from both sides of the leaf
Emmitt=-2*emmiss*SBconst*(Tleaf+273.15)^4;

% 0 assuming no reflectance (soil not visible under closed canopy)
SolarRad=SolarRadWM2*(1-albedo)*(1+0);

% Campbell and Norman (1998)(Multipy 2 for both side of leaf: top side gain
% thermal radiation from the sky and bottom from the surrouding: here we
% consider the sky and surrounding has the same temeprature)
IRRad=2*1.72*((ea/(TaoC+273.15))^(1/7))*SBconst*((TaoC+273.15)^4);

NetRad=SolarRad+IRRad+Emmitt;

% corrections according to FAO56 due to heat storage by soil during day and
% loss during night
if(NetRad>0)
    NetRad=NetRad-0.1*NetRad;
else
    NetRad=NetRad-0.5*NetRad;
end
x = ((NetRad+Lat+Conv)^2);
