% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016
%A supply equation for transpiration derived from plant hydraulics

function x = TRSupplyFunc(kleafa,kstema,kroota,ksoila,LeafWPa,SoilWPa)
x=-((1/kleafa+1/kstema+1/kroota+1/ksoila)^-1)*(LeafWPa-SoilWPa);
% Output: mmol m-2 s-1
 