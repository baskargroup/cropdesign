function x = AnC3Cc,VcmaxTleaf,JTleaf,TPUTleaf,RdTleaf,KcTleaf,KoTleaf,gammastarTleaf)
%Calculation of C3 An
Ac=VcmaxTleaf*((Cc)-gammastarTleaf)/((Cc)+(KcTleaf*10)*(1+21/KoTleaf))-RdTleaf;
%Oxy in kPa (21)
Aj=JTleaf*(((Cc)-gammastarTleaf)/(4*(Cc)+8*gammastarTleaf))-RdTleaf;
At=3*TPUTleaf-RdTleaf;
x=min(Ac,Aj,At);
if(x< -RdTleaf)
    x=-RdTleaf;
end
