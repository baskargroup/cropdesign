function x = AnC3squared(Cc)
AnFicks=FicksAnC3(Ca,Cc,gblc,gst,gmTleaf,Pressure)
AnFvCB=AnC3(Cc,VcmaxTleaf,JTleaf,TPUTleaf,RdTleaf,KcTleaf,KoTleaf,gammastarTleaf);
x = ((AnFicks-AnFvCB)^2);

