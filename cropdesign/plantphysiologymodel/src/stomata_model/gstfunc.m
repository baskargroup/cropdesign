% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016
% This function calculates stomtal conductance based on sigmodal response
% of the stomatal conductance with respect to the leaf water potential.

function x = gstfunc(gmin,gmax,LWP,gstSensitivity,gstThres,gstABAsens,RootWP,An,gmaxAnP1,gmaxAnP2)
%gst ; stomatal conductance to water vapor response to leaf, root water potential and photosynthesis
% The gst to LWP response is roughly based upon equation 9.13 in Campbell and Norman (1998)
% The gst to LWP response is adjusted by a linear increase in sensitivity to RootWP. Note: the default is to set gstABAsens to 0, removing any effect of ABA
% The gst to LWP response further adjusted by inclusion of a calculated gstmidpoint, this leads to partial mathematical independence of the effects of gstThres and gstSensistivity on the shape of the response.
%The gst to An response is based upon a power function rather than proportionality (Wong et al. 1979) as Ci in reality is not kept constant, i.e. An has diminishing returns for increases in gst.
%However the empirical exponential relationship between gst and An, (An=m*ln(gst)+c reported in Gilbert et al. (2011) Journal of Experimental Botany), was found to not be ideal as it leads to poor functioning of the numerical solver.

if(An<= 0)
    y=0.0001;
else
    y=An;
end

gmaxAn=gmaxAnP1*y^gmaxAnP2;

if(gmaxAn> gmax)
    gmaxAn=gmax;
end

gstMidpoint=(((gmaxAn)/(0.95*gmaxAn)-1)^(1/(gstSensitivity-RootWP*gstABAsens))); 

x=(gmaxAn)/(1+(LWP/(gstThres/gstMidpoint))^(gstSensitivity-RootWP*gstABAsens))+gmin;

