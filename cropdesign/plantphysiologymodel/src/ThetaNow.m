% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function calculate hourly the soil water content based on the
% transpiration data

function x = ThetaNow(Irrig,InputParams,CurrentWeather,OutPutValsPrev)

ThetaSat = InputParams.ThetaSat;
SoilDepth = InputParams.SoilDepth;
SoilRadius = InputParams.SoilRadius;
LAI = InputParams.LAI;
SoilDef = InputParams.SoilDef;

Precip = CurrentWeather(5);

TRPrev= OutPutValsPrev.TR;
ThetaPrev = OutPutValsPrev.Theta;

%Soil water content calculation
Volume=SoilDepth*pi*SoilRadius^2;

%Setting SoilDef in the ConstantList file to 0 results in no change in soil water content
x=ThetaPrev+SoilDef*((Precip*1000*pi*SoilRadius^2-LAI*TRPrev*(18/1000)*3600*pi*SoilRadius^2+Irrig)/(1000000*Volume));

if (x>= ThetaSat)
    x=ThetaSat;
end

