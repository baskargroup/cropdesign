% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function read input variables from input csv file

function InputParams = ReadInputParams(filename, row)
fid = fopen(filename, 'r');
for i=1:row-1
tline = fgetl(fid);
end

tline = fgetl(fid);
% Parse and read rest of file
A = regexp(tline, '\,', 'split'); 
fclose(fid);
% minimum stomatal conductance to water vapor including epidermal
% conductance
InputParams.gmin=  str2double(cell2mat(A(1)));  %mol m-2 s-1
% maximum stomatal conductance to water vapor
InputParams.gmax=  str2double(cell2mat(A(2))); %mol m-2 s-1 
% slope of the relationship between gst and leaf water potential
InputParams.gstSensitivity= tan(str2double(cell2mat(A(3)))*pi/180);  %mol m-2 s-1 MPa-1  
% threshold bulk leaf water potential at stomatal closure
InputParams.gstThres= str2double(cell2mat(A(4)));  %MPa
% slope of the relationship between gst and and root water potential
% (approximate response to ABA)
InputParams.gstABAsens= tan(str2double(cell2mat(A(5)))*pi/180);  %mol m-2 s-1 MPa-1
% leaf hydraulic conductance to water
InputParams.kleaf=str2double(cell2mat(A(6)));  %mmol m-2 s-1 MPa-1
%stem hydraulic conductance to water
InputParams.kstem=str2double(cell2mat(A(7)));  %mmol m-2 s-1 MPa-1
% root hydraulic conductance to water
InputParams.kroot=str2double(cell2mat(A(8)));  %mmol m-2 s-1 MPa-1
%%% Pressure
InputParams.Pressure =  str2double(cell2mat(A(9)));  
%%PEP regeneration rate   umol m-2 s-1
InputParams.Vpr25 = str2double(cell2mat(A(10)));
%%PEP maximum carboxylation rate  umol m-2 s-1
InputParams.Vpmax25 = str2double(cell2mat(A(11)));
% Rubisco maximum carboxylation rate  umol m-2 s-1
InputParams.Vcmax25 = str2double(cell2mat(A(12)));
%%%
% InputParams.TPU25 = str2double(cell2mat(A(14)));
% Day respiration  umol m-2 s-1
InputParams.Rd25 = str2double(cell2mat(A(13)));
%%%
InputParams.gm25 = str2double(cell2mat(A(14)));
%Light saturated electron transport rate  umol m-2 s-1
InputParams.Jmax25 = str2double(cell2mat(A(15)));
%light response PSII to PSI proportioning factor
InputParams.PSIIPAR = str2double(cell2mat(A(16)));
%light response curvature factor     
InputParams.curvePAR = str2double(cell2mat(A(17)));

%%%
InputParams.SoilKoff = str2double(cell2mat(A(18)));
%%%Soil texture dependent parameter
InputParams.Soilb= str2double(cell2mat(A(19)));
%%%Saturated soil hydraulic conductivity
InputParams.Ksat= str2double(cell2mat(A(20)));  
%%%%Saturated soil water potential
InputParams.SoilWPSat= str2double(cell2mat(A(21)));  %
% soil volumetric water content at start of growing season
InputParams.ThetaStart=str2double(cell2mat(A(22)));  %m3 m-3 
% soil volumetric saturation
InputParams.ThetaSat=str2double(cell2mat(A(23)));  %m3 m-3
% depth of rooting volume
InputParams.SoilDepth=str2double(cell2mat(A(24)));  %m
% radius of soil cylinder plant occupies (~25 plants per m-2)
InputParams.SoilRadius=str2double(cell2mat(A(25)));  %m
% root length per unit volume of soil
InputParams.RootLengthDensity=str2double(cell2mat(A(26)));  %  m root m-3
% radius of a root
InputParams.RootRadius= str2double(cell2mat(A(27)));  %m   
%ground area % leaf area index
InputParams.LAI=str2double(cell2mat(A(28)));  %m2 LA m-2 
% canopy height
InputParams.CropHeight=str2double(cell2mat(A(29)));  %m
% density of dry soil
InputParams.SoilDensitygm3=str2double(cell2mat(A(30)));  %g m-3=

% turns on reductions in soil water due to transpiration
InputParams.SoilDef=str2double(cell2mat(A(31)));  %                  flag  
%%% leaf temperature
InputParams.LeafTemp = str2double(cell2mat(A(32)));
% turns on irrigation every "Irrigate" number of days
% '0' is no irrigation
InputParams.Irrigate=str2double(cell2mat(A(33))); %          in days
% leaf average width
InputParams.LeafDimension=str2double(cell2mat(A(34)));  %m 
% leaf water potential at permanent wilting point
InputParams.PermWiltPt=str2double(cell2mat(A(35)));  %MPa
% leaf temperature at permanent leaf damage
InputParams.PermTempDamage=str2double(cell2mat(A(36)));  %oC
%parameter related with photosynthesis or CO2 assimilation
InputParams.gmaxAnP1 = str2double(cell2mat(A(37))); %0.000787/0.0036
%parameter related with photosynthesis or CO2 assimilation
InputParams.gmaxAnP2 = str2double(cell2mat(A(38))); %2.06/1.11693
% Q10 coefficient Conversion factor related to PEP regeneration rate calculation
InputParams.VprQ10=str2double(cell2mat(A(39)));% #95% CI is 13.1 to 23.4          # This is unrealistically high, but is poorly constrained in the model. Vpr doesn't then limit photosynthesis at high temperature which is probably fine.
%Q10 coefficient Conversion factor related with rubisco carboxylation rate calculation
InputParams.VcmaxQ10=str2double(cell2mat(A(40)));%#95% CI is 0.914 to 1.065      # A little low, but probably not a problem. The resultant temperature responses are reasonable
% parameter related with rubisco carboxylation rate
InputParams.Vcmaxa=str2double(cell2mat(A(41)));% #95% CI is 0.0806 to 0.100
% parameter related with rubisco carboxylation rate
InputParams.Vcmaxb=str2double(cell2mat(A(42)));% #95% CI is 31.1 to 32.4
% parameter related with rubisco carboxylation rate
InputParams.Vcmaxc=str2double(cell2mat(A(43)));%#95% CI is  38.7 to 44.1
% Conversion factor related with PEP carboxylation rate calculation
InputParams.VpmaxQ10=str2double(cell2mat(A(44)));%#95% CI is 1.37 to 1.52
% Q10 coefficient Conversion factor related to mitochondrial respiration calculation 
InputParams.RdQ10=str2double(cell2mat(A(45)));%#95% CI is 1.55 to 2.31
% parameter related with electron transport rate
InputParams.Ja=str2double(cell2mat(A(46)));%#95% CI is  4.93 to 6.7
% parameter related with electron transport rate
InputParams.Jb=str2double(cell2mat(A(47)));%#95% CI is 3.65 to 8.22
%  is a fitting parameter related to photosynthesis rate 
InputParams.alpha=str2double(cell2mat(A(48)));%#95% CI is 0.0727 to 0.1068





