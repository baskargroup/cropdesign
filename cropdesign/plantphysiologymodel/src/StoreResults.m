% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016
% This function stores the hourly output in a csv file
function flag = StoreResults(data)

A = struct2cell(data);
B = cell2mat(A);

header = {'gst', 'TR', 'LeafWP', 'StemWP','RootWP','SoilWPatRoot','ksoil','Theta','ThetaPrev','SoilWP','An','PCO','CcCi','VPDmmol','PAR','Irrigmm','rs','ra','Tleaf','Rad','Latent','Convec','Emmit','Ta','ETmm','PMETomm'};
filename = sprintf('outcome.csv');
csvwrite_with_headers(filename,B',header);