% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function reads hourly weather data from csv file that contains
% weather data
function AllWeather = ReadWeatherFile(filename)

AllWeather = csvread(filename,1,1);
