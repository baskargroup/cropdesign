% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016
% This function estimate wind speed on canopy from avaible wind speed ( probably measured above a
% certain height of the canopy. 

function Uz = WindCanopy(WindSpeedat2m,CropHeight)
% Correction for wind speed reduction to Canopy height according to
% Baldocchi et al. 1983
%shearing velocity is calculated from the measured speed at 2 m height (modification 2).
ustar=(WindSpeedat2m*0.4)/log((2-0.6*CropHeight)/0.07*CropHeight);
%considering shearing velocity will be the same, the wind speed just over
%the canopy can be calculated as,(modification 2)
Uz=(ustar/0.4)*log((CropHeight-0.6*CropHeight)/0.07*CropHeight);

% Set so that temperature doesn't increase unrealistically at high
% temperatures due to lack of convection. This is recommended practice in FAO 56.
if(Uz<= 0.5)
    Uz=0.5;          
end
    
    