% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function evaluates LWP by balacing liquid water supplied to the leaf, TRsupply 
% and water vapor demand by the atomosphere, TRdemand.

function x = TRsquared(LWP, Params)
InputParams = Params{1};
CurrentWeather = Params{2};
GlobalConsts = Params{3};
HourlyConstants = Params{4};
OutputValsPrev = Params{5};
OutputValsCurr = Params{6};

kleaf = InputParams.kleaf;
kstem = InputParams.kstem;
kroot = InputParams.kroot;
SoilKoff = InputParams.SoilKoff;
Ksat = InputParams.Ksat;
SoilWPSat = InputParams.SoilWPSat;
Soilb = InputParams.Soilb;
RootLengthDensity = InputParams.RootLengthDensity;
SoilDepth = InputParams.SoilDepth;
SoilRadius = InputParams.SoilRadius;
LAI = InputParams.LAI;
RootRadius = InputParams.RootRadius;
gblc = HourlyConstants.gblc;
ea = HourlyConstants.ea;
gst = Params{7};
Tleaf = Params{8};
Theta = OutputValsCurr.Theta;
SoilWP = OutputValsCurr.SoilWP;

es=0.6107*exp(17.38*Tleaf/(Tleaf+239)); %Saturation vapor pressure (kPa)
VPD=(es-ea)*1000/InputParams.Pressure;  %leaf to air vapor pressure deficit (mmol H2O mol-1 air)
if(VPD< 0)
    VPD=0;
end

% Water vapor demand based on the weather data
TRDemand = TRDemandFunc(gst,gblc,VPD);
% Water potential at the root soil interface
SoilWPatRoot1=SoilWPatRoot(LWP,TRDemand,kleaf,kstem,kroot,SoilWP);
% Conductance of the soil
ksoil=ksoilCampbell(SoilKoff,Ksat,SoilWPSat,SoilWP,SoilWPatRoot1,Soilb,RootLengthDensity,SoilDepth,SoilRadius,LAI,RootRadius);

% Liquid water supplied to leaf from soil
TRSupply = TRSupplyFunc(kleaf,kstem,kroot,ksoil,LWP,SoilWP);
x=(TRSupply-TRDemand)^2;

