% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% This function evaluates water potential at soil-root interface using
% iterative method
function x = SoilWPatRoot(LeafWP,TRdemand,kleaf,kstem,kroot,SoilWP)
kplant=(1/kleaf+1/kstem+1/kroot)^-1;
x=TRdemand/kplant+LeafWP;
if(x>SoilWP)
    x=SoilWP-0.0000001;
end

