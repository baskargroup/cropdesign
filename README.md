# Crop Design in Silico #

A hydraulic model of crop physiology is developed to guide trait selection in crop breeding programs. The physiological model describes the transient water use (over 60 days) of in silico plant with various combinations of traits. A parallel evolutionary optimization algorithm is integrated with the physiological model to identify traits that maximize productivity in irrigated conditions, and enhance survival under drought.

CropDesign is compatible with: __MATLAB2018b__.


------------------


## Guiding principles

- __User friendliness.__ 

- __Modularity.__ 

- __Easy extensibility.__ 

- __Work with MATLAB__. 


------------------


## Getting started

- To evaluate the performace of a known plant in a specific environment

"./cropdesign/plantphysiologymodel/Main_singlePlant" that is main function to execute the model. "src" contains supporting functions. "input" contains weather data in "weather" and and input parameters including plant traits, agronomonic management, etc. in "InputParam".  
  
  a)To see the performace of a plant for a specific location, replace the environmental information in ./cropdesign/plantpyhysiologymodel/inputs/Weather.csv. 
  
  b)To change the traits of the plant, replace the plantphysiological parameters in ./cropdesign/plantpyhysiologymodel/inputs/InputParam.csv. 
  
  c)Run the Main_singlePlant.m script
  
  d)The performance will be outputed as ./cropdesign/plantpyhysiologymodel/inputs/outcome.csv. To name of the output file can be changed via ./cropdesign/plantphysiologymodel/src/StoreResults.m
  
- Use PlantPhysiolgyOptimization to optimize plant traits (to design ideotype) for a specific environment. 
"MainOptimize" in this folder that is main function to execute the optimization. "src" contains supporting functions."input" contains weather data in "weather" and input parameters including plant traits, agronomonic management, etc. in "InputParam". "CostFunction" contains the objective function for the optimization. 

  a) To optimize a plant for a specific location replace the data in "weather" file with the weather data of that place.
 
  b) To change the traits of the plant, soil type, irrigation frequency, etc. open the "InputParam" file, and replace the values as required. 

  c) To run the MainOptimize function type in the command window  with appropriate arguments. e.g."MainOptimize 45 7 10 100 5 1e-4 1 2 1 2". 
     The description of the arguments can be found inside the funciton.
     Here, 45 7 10 100 5 1e-4 1 2 1 2 the arguments represent
     
     theta: the weighting factor between two preliminary objective functions 
     gennum: maximum of generations, as stopping criteria. 
     popsize: initial population.
     itr1: start iteration % iteration number will help to create different seeds
     itr2: Finish iteration 
     condition : bound or unbound traits space for conductactivity
     nop : number of processors used 
     stallgen : no of stall generations that occur as a stopping criteria
     Irrfre : irrigation frequency
     tolfun : Tolerance 
     
  d) All functions contain other supporting information. 

------------------
## Authors
[Zaki Jubery](mailto:znjubery@gmail.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Feel free to raise issues and contribute to the Software.

##Contact:
Baskar Ganapathysubramanian

Mechanical Engineering

Iowa State University

baskarg@iastate.edu


