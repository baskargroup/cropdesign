function [S,T] = Ideo_descriptivestats(Traits_all,AnIr,AnDr,factor)
%This function collection all the plants near the ideotypes and generate descriptive statistics
% of the triats
% Here near is defined by the plants which An under irrigation and drought with in 5% of those of the ideotype
Selected_v=find(abs(Traits_all(:,8)-AnIr)<factor*AnIr & abs(Traits_all(:,11)-AnDr)<factor*AnDr);
Traits_Id=Traits_all(Selected_v,:);
Traits=Traits_Id(:,1:7);
Mean=mean(Traits);
S.Mean=Mean;
%S.Median=median(Traits);
%S.Var=var(Traits);
Std=std(Traits);
S.Std=Std;

% Create Cell with mean+-std as entry
T=cell(1,7);
for i=1:7
    if i==1
    dp=3;
    else
    dp=2;    
    end
    A=[num2str(round(Mean(i),dp)) '' char(177) '' num2str(round(Std(i),dp))];
    T{1,i}=A;
end

end

