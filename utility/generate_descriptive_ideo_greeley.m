%December 29, 2015
% Zaki Jubery
close all
clear all
clc
%% Readme 
%change the addpath based on the current pc or
%load 'Davis_all_data.mat' from Optimization_outcome folder
%% Load data ( use meisu for baskar pc, AttingerLab for office laptop)
addpath ('..\Optimization_outcome\forGreeley2004')
load('Greeley_data_davis_pareto.mat')
%% Removing data that didn't satisfy conservation laws
C_v=find(Traits1(:,14)<1e-3 & Traits1(:,15)<1e-3 & Traits1(:,16)<1e-3 & Traits1(:,17)<1e-3 & Traits1(:,20)<1e-3 & Traits1(:,21)<1e-3 & Traits1(:,22)<1e-3 & Traits1(:,23)<1e-3);
Traits_cv=Traits1(C_v,:);
Traits_all=Traits_cv;
AA0s=find(Traits_cv(:,13)==1464);
STraits_all=Traits_cv(AA0s,:);

% Generate descriptive statistics of the ideotypes
AnIr=22.195;
AnDr=21.95;

S_Stng = Ideo_descriptivestats(STraits_all,AnIr,AnDr);

AnIr=37.40;
AnDr=36.63;
S_drought = Ideo_descriptivestats(STraits_all,AnIr,AnDr);


AnIr=61.838;
AnDr=39.29;

S_Smart = Ideo_descriptivestats(STraits_all,AnIr,AnDr);

AnIr=54.66;
AnDr=47.86;

S_Extra = Ideo_descriptivestats(Traits_all,AnIr,AnDr);

T = struct2table([S_Stng,S_drought,S_Smart,S_Extra]);
T.Properties.RowNames = {'Sting','Drought','Smart','Extravagant'};

save('greeley_ideotype.mat')