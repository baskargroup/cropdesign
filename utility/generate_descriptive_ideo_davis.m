%December 29, 2015
% Zaki Jubery
close all
clear all
clc
%% Readme 
%change the addpath based on the current pc or
%load 'Davis_all_data.mat' from Optimization_outcome folder
%% Load data ( use meisu for baskar pc, AttingerLab for office laptop)
addpath ('..\Optimization_outcome\forDavis_2010')
load('Davis_data_davis_pareto.mat')
%% Removing data that didn't satisfy conservation laws
C_v=find(Traits1(:,14)<1e-3 & Traits1(:,15)<1e-3 & Traits1(:,16)<1e-3 & Traits1(:,17)<1e-3 & Traits1(:,20)<1e-3 & Traits1(:,21)<1e-3 & Traits1(:,22)<1e-3 & Traits1(:,23)<1e-3);
Traits_cv=Traits1(C_v,:);
Traits_all=Traits_cv;
AA0s=find(Traits_cv(:,13)==1464);
STraits_all=Traits_cv(AA0s,:);

% Generate descriptive statistics of the ideotypes
AnIr=30.75;
AnDr=30.58;
[S_Stng,C_Stng] = Ideo_descriptivestats(Traits_all,AnIr,AnDr,0.1);

AnIr=53.077;
AnDr=38.878;
[S_drought,C_drought] = Ideo_descriptivestats(Traits_all,AnIr,AnDr,0.1);

AnIr=81.00;
AnDr=34.46;
[S_Smart,C_Smart] = Ideo_descriptivestats(Traits_all,AnIr,AnDr,0.1);

AnIr=88.64;
AnDr=23.74;
[S_Extra,C_Extra] = Ideo_descriptivestats(Traits_all,AnIr,AnDr,0.1);

T = struct2table([S_Stng,S_drought,S_Smart,S_Extra]);
T.Properties.RowNames = {'Stingy','Drought','Smart','Extravagant'};

Header={'gmin',	'gmax','Sl','th','Sr','Kshoot','Kroot'};
C_all = [C_Stng; C_drought;C_Smart;C_Extra];
T_std = cell2table(C_all,'VariableNames',Header);
T_std.Properties.RowNames = {'Stingy','Drought','Smart','Extravagant'};

save('p_davis_ideotype.mat')

writetable(T_std,'p_davis_ideotype_std.xlsx','Sheet',1,'WriteVariableNames',true);
writetable(T,'p_davis_ideotype.xlsx','Sheet',1,'WriteVariableNames',true);