% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% The model is adapted from the plant physiology model 
% by Matthew Gilbert, UC Davis, CA. 

% add path to the folders "inputs" and "src". "Inputs" contains files of 
% weather file and plant traits, and "src" contains various functions of 
% the model implementation. 
clc;
close all;
clear all;


maindir='G:\CropDesign\FoodEnergySecurityJournal\cropdesign\cropdesign\plantphysiologymodel';

% Location of source code
addpath (fullfile(maindir,'src'))
addpath (fullfile(maindir,'src','stomata_model'))
addpath (fullfile(maindir,'src','C4Model'))
% Remove path of exsiting gst function 
% rmpath (fullfile(maindir,'src','stomata_model'))

%  Location of input files (Weather and Plant Traits)
addpath (fullfile(maindir,'inputs'))


Transpri=zeros(24,1);
Assimil=zeros(24,1);
survival=zeros(24,1);

for i=1:24
filename = 'Weather.csv'; % reading weather file
AllWeather= ReadWeatherFile(filename);

%Consider only 30*24 hours 
%AllWeather(30*24:end,:)=[];


filename = 'InputParam.csv'; % reading plant traits
%row2read, row 1 is header
row2read = 2;
InputParams = ReadInputParams(filename,row2read);

% Set no irrigation
InputParams.Irrigate=7;

% Typical vlaues for maize (check paper_outline_july21.docx)
InputParams.gmin=0.02;
InputParams.gmax=0.2555;
InputParams.gstSensitivity=15;
InputParams.gstThres=-1.25;
InputParams.gstABAsens=200;
InputParams.kleaf=45;
InputParams.kroot=45;
InputParams.kstem=45;


% Inputs uncertainity (epistemic uncertainity)
%parameter related with photosynthesis or CO2 assimilation
if i==1;InputParams.gmaxAnP1 = 0.000787;end;
if i==2;InputParams.gmaxAnP1 =0.0036;end;
%parameter related with photosynthesis or CO2 assimilation
if i==3;InputParams.gmaxAnP2 = 2.06;end;
if i==4;InputParams.gmaxAnP2=1.11693;end;
% Q10 coefficient Conversion factor related to PEP regeneration rate calculation
if i==5;InputParams.VprQ10= 13.1;end;
if i==6;InputParams.VprQ10=23.4;end;          % This is unrealistically high, but is poorly constrained in the model. Vpr doesn't then limit photosynthesis at high temperature which is probably fine.
%Q10 coefficient Conversion factor related with rubisco carboxylation rate calculation
if i==7;InputParams.VcmaxQ10=0.914;end;
if i==8;InputParams.VcmaxQ10=1.065;end;      % A little low, but probably not a problem. The resultant temperature responses are reasonable
% parameter related with rubisco carboxylation rate
if i==9;InputParams.Vcmaxa=0.0806;end; 
if i==10;InputParams.Vcmaxa=0.100;end;
% parameter related with rubisco carboxylation rate
if i==11;InputParams.Vcmaxb=31.1;end;
if i==12;InputParams.Vcmaxb=32.4;end;
% parameter related with rubisco carboxylation rate
if i==13;InputParams.Vcmaxc=38.7;end;
if i==14;InputParams.Vcmaxc=44.1;end;
% Conversion factor related with PEP carboxylation rate calculation
if i==15;InputParams.VpmaxQ10=1.37;end;
if i==16;InputParams.VpmaxQ10=1.52;end;
% Q10 coefficient Conversion factor related to mitochondrial respiration calculation 
if i==17;InputParams.RdQ10=1.55;end;
if i==18;InputParams.RdQ10=2.31;end;
% parameter related with electron transport rate
if i==19;InputParams.Ja=4.93;end;
if i==20;InputParams.Ja=6.7;end;
% parameter related with electron transport rate (95% CI)
if i==21;InputParams.Jb=3.65;end;
if i==22;InputParams.Jb=8.22;end;
%  is a fitting parameter related to photosynthesis rate (95% CI)
if i==23;InputParams.alpha=0.0727;end;
if i==24;InputParams.alpha=0.1068;end;

%set global constants
GlobalConsts = SetGlobalConsts();

%solve 
resultsA = CropSimulator(AllWeather, InputParams, GlobalConsts);
results = resultsA{1};
CropDied = resultsA{2};

%store results into a csv file 
StoreResults(results);
 
%postprocess results
ppresults = PostProcResults(results);
Transpri(i)=ppresults(1)*3600*1e-3% in mol/m2
Assimil(i)=ppresults(2)*3600*1e-6% in mol/m2
survival(i)=CropDied(2)
clear InputParams
end
save('workspacevariable.mat')