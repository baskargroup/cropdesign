function x = gstfunc(gmin,gmax,LWP,gstSensitivity,gstThres,gstABAsens,RootWP,An,gmaxAnP1,gmaxAnP2,Cs,Rh)
% implementation of Bell_Berry model
%Rh relative humidity
%Cs  CO2 concentration on the leaf

m=7.0; % Stomatal slope, Phong et al. 2011
x=gmin+m*An*Rh/Cs;

