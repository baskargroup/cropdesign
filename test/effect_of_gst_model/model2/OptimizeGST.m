function x = OptimizeGST(gst, paramcell)

InputParams = paramcell{1};
%CurrentWeather = paramcell{2};
GlobalConsts = paramcell{3};
HourlyConstants = paramcell{4};
%OutputValsPrev = paramcell{5};
%OutputValsCurr = paramcell{6};

%Tleaf = EBsquared(Tleaf, paramcell);
%Find Tleaf minimizing energy balance^2 given gst
%%paramcellnew = paramcell;
%%paramcellnew{7} = gst;
paramcell{7} = gst;%%
%%[Tleaf,fval] = fminbnd(@(x) EBsquared(x,paramcellnew),-10,100);
[Tleaf,fval] = fminbnd(@(x) EBsquared(x,paramcell),-10,100);%%

%%paramcellnew{8} = Tleaf;
paramcell{8} = Tleaf;%%
%Find LWP minimizing (TRSupply-TRDemand)^2 given gst, Tleaf
%%[LWP, feval] = fminbnd(@(x) TRsquared(x, paramcellnew),-5,5);
[LWP, feval] = fminbnd(@(x) TRsquared(x, paramcell),-5,5);%%

%%paramcellnew{9} = LWP;
paramcell{9} = LWP;%%
%Find Ci, An minimizing (gst-gst(An)) given gst, Tleaf, LWP
Ca = GlobalConsts.Ca;
%%[Ci, feval] = fminbnd(@(x) AnC4squared(x, paramcellnew), 0,Ca*3);
[Ci, feval] = fminbnd(@(x) AnC4squared(x, paramcell), 0,Ca*3);%%
gblc = HourlyConstants.gblc;
An=FicksAnC4(Ca,Ci,gblc,gst);

%Find updated gst
gmin = InputParams.gmin;
gmax = InputParams.gmax;
gstSensitivity = InputParams.gstSensitivity;
gstThres = InputParams.gstThres;
gstABAsens = InputParams.gstABAsens;

gmaxAnP1 = InputParams.gmaxAnP1;
gmaxAnP2 = InputParams.gmaxAnP2;

%get RootWP
%[TRdemand, SoilWpatRoota, RootWP] = GetRootWP(paramcellnew);
[TRdemand, SoilWpatRoota, RootWP] = GetRootWP(paramcell);%%
RH=HourlyConstants.RH;
gstnew = gstfunc(gmin,gmax,LWP,gstSensitivity,gstThres,gstABAsens,RootWP,An,gmaxAnP1,gmaxAnP2,Ca,RH);
x=gst-gstnew;
% x = (gst-gstnew)^2/(min(gst,gstnew)+1e-25)^2; %(Modified by Zaki, July 19, 2014; change the converange creteria for stomatal conductance, it was giving unwanted stomatal closure)