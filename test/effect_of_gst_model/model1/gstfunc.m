% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2018
% This function calculates stomtal conductance based on sigmodal response
% of the stomatal conductance with respect to the leaf water potential.

function x = gstfunc(gmin,gmax,LWP,gstSensitivity,gstThres,gstABAsens,RootWP,An,gmaxAnP1,gmaxAnP2)
% keeping the original structure of the function we implemented now model
% here
% New model
% gst; only response to the light,Daytime full open, nighttime fully closed
% This is implemented by gst=gmin when An<=0 and gst=gmax at An>0 at daytime
if(An<= 0)
    x=gmin;
else
    x=gmax;
end