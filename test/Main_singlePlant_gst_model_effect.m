% Developers : Baskar Ganapathysubramanian & Talukder "Zaki" Jubery
% Copyright : Baskar Ganapathysubramanian, 2016

% The model is adapted from the plant physiology model 
% by Matthew Gilbert, UC Davis, CA. 

% add path to the folders "inputs" and "src". "Inputs" contains files of 
% weather file and plant traits, and "src" contains various functions of 
% the model implementation. 
clc;
close all;
clear all;


maindir='G:\CropDesign\FoodEnergySecurityJournal\cropdesign\cropdesign\plantphysiologymodel';

% Location of source code
addpath (fullfile(maindir,'src'))
addpath (fullfile(maindir,'src','stomata_model'))

% Remove path of exsiting gst function 
% rmpath (fullfile(maindir,'src','stomata_model'))

%  Location of input files (Weather and Plant Traits)
addpath (fullfile(maindir,'inputs'))

filename = 'Weather.csv'; % reading weather file
AllWeather= ReadWeatherFile(filename);

%Consider only 30*24 hours 
%AllWeather(30*24:end,:)=[];


filename = 'InputParam.csv'; % reading plant traits
%row2read, row 1 is header
row2read = 2;
InputParams = ReadInputParams(filename,row2read);

% Set no irrigation
InputParams.Irrigate=7;

% Typical vlaues for maize (check paper_outline_july21.docx)
InputParams.gmin=0.02;
InputParams.gmax=0.2555;
InputParams.gstSensitivity=15;
InputParams.gstThres=-1.25;
InputParams.gstABAsens=200;
InputParams.kleaf=45;
InputParams.kroot=45;
InputParams.kstem=45;


%set global constants
GlobalConsts = SetGlobalConsts();

%solve 
resultsA = CropSimulator(AllWeather, InputParams, GlobalConsts);
results = resultsA{1};
CropDied = resultsA{2};

%store results into a csv file 
 StoreResults(results);
 
%postprocess results
ppresults = PostProcResults(results);
Transpri=ppresults(1)*3600*1e-3% in mol/m2
Assimil=ppresults(2)*3600*1e-6% in mol/m2
survival=CropDied(2)

save('workspacevariable.mat')